import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe
import math.IPolynomial
import math.Polynomial
import math.utils.PolynomialArgumentException
import math.utils.equalsWithEpsilon
import kotlin.test.assertTrue


internal class PolynomialScenarioTest : BehaviorSpec({
    lateinit var polynomial: IPolynomial
    val defaultEpsilon = 0.0001

    beforeEach {
        polynomial = Polynomial()
    }

    Given("x^2 + 1 = 0") {
        When("no equation roots") {
            val a = 1.0
            val b = 0.0
            val c = 1.0

            Then("return empty list") {
                polynomial.solve(a, b, c) shouldBe emptyList()
            }
        }
    }

    Given("x^2 - 1 = 0") {
        When("equation have 2 roots") {
            val a = 1.0
            val b = 0.0
            val c = -1.0

            Then("return list of two elements: 1.0, -1.0") {
                assertTrue {
                    polynomial.solve(a, b, c).equalsWithEpsilon(
                        other = listOf(1.0, -1.0),
                        epsilon = defaultEpsilon
                    )
                }
            }
        }
    }

    Given("x^2 + 2x + 1 = 0") {
        When("equation have 1 root") {
            val a = 1.0
            val b = 2.0
            val c = 1.0

            Then("return list of two elements: -1.0, -1.0") {
                assertTrue {
                    polynomial.solve(a, b, c).equalsWithEpsilon(
                        other = listOf(-1.0, -1.0),
                        epsilon = defaultEpsilon
                    )
                }
            }
        }
    }

    Given("polynomial with a, b, c arguments") {
        When("a argument equals 0") {
            val a = 0.0
            val b = 2.0
            val c = 1.0

            Then("throw exception") {
                shouldThrow<PolynomialArgumentException> {
                    polynomial.solve(a, b, c)
                }
            }
        }
    }

    Given("x^2 + 3x - 10 = 0") {
        val a = 1.0
        val b = 3.0
        val c = -10.0

        Then("expected result is list of 2, 5") {
            assertTrue {
                polynomial.solve(a, b, c).equalsWithEpsilon(
                    other = listOf(2.0, -5.0),
                    epsilon = defaultEpsilon
                )
            }
        }
    }

    Given("7x^2 + 3x + 1 = 0") {
        val a = 7.0
        val b = 3.0
        val c = 1.0

        Then("result lust of -19, -19") {
            polynomial.solve(a, b, c) shouldBe emptyList()
        }
    }

    Given("6x^2 + 2x - 6 = 0") {
        val a = 6.0
        val b = 2.0
        val c = -6.0

        Then("result list of doubles with 3 zeros after dot") {
            assertTrue {
                polynomial.solve(a, b, c).equalsWithEpsilon(
                    other = listOf(0.847, -1.18),
                    epsilon = defaultEpsilon
                )
            }
        }
    }

    Given("discriminant not equals zero like 0.0000124") {
        val a = 2.0
        val b = -4.000001
        val c = 2.0

        When("discriminant contains in epsilon range") {
            Then("consider that discriminant equals zero") {
                assertTrue {
                    polynomial.solve(a, b, c).equalsWithEpsilon(
                        other = listOf(1.0, 1.0),
                        epsilon = defaultEpsilon
                    )
                }
            }
        }
    }

    Given("Double is Nan") {
        When("a argument is NaN") {
            val a = Double.NaN
            val b = 2.0
            val c = 4.0

            Then("throw exception") {
               shouldThrow<PolynomialArgumentException> {
                    polynomial.solve(a, b, c)
                }
            }
        }

        When("d argument is NaN") {
            val a = 1.0
            val b = Double.NaN
            val c = 4.0

            Then("throw exception") {
                shouldThrow<PolynomialArgumentException> {
                    polynomial.solve(a, b, c)
                }
            }
        }

        When("c argument is NaN") {
            val a = 1.0
            val b = 3.0
            val c = Double.NaN

            Then("throw exception") {
                shouldThrow<PolynomialArgumentException> {
                    polynomial.solve(a, b, c)
                }
            }
        }

        When("any of arguments is NaN") {
            val arguments = listOf(
                5.6,
                3.4,
                Double.NaN
            ).shuffled()

            Then("throw exception") {
                shouldThrow<PolynomialArgumentException> {
                    polynomial.solve(
                        a = arguments.component1(),
                        b = arguments.component2(),
                        c = arguments.component3(),
                    )
                }
            }
        }
    }

    Given("Infinity double value") {
        When("any of the arguments is negative infinity") {
            val a = 1.1
            val b = 3.0
            val c = Double.POSITIVE_INFINITY

            Then("throw exception") {
                shouldThrow<PolynomialArgumentException> {
                    polynomial.solve(a, b, c)
                }
            }
        }

        When("any of the arguments is positive infinity") {
            val a = 4.0
            val b = Double.NEGATIVE_INFINITY
            val c = 0.1

            Then("throw exception") {
                shouldThrow<PolynomialArgumentException> {
                    polynomial.solve(a, b, c)
                }
            }
        }
    }
})

