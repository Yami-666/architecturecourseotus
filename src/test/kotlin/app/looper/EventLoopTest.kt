package app.looper

import app.looper.exception_commands.DoubleTryOrLogExceptionCommand
import app.looper.exception_commands.LogExceptionCommand
import app.looper.exception_commands.OnceTryOrLogExceptionCommand
import app.looper.exception_commands.TryExceptionCommand
import io.kotest.core.spec.style.BehaviorSpec
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify

class EventLoopTest : BehaviorSpec({
    lateinit var looper: EventLooper

    beforeSpec {
        looper = spyk(EventLooper())
    }

    // 4.
    Given("WriteLogCommand") {
        val writeLogCommand = mockk<WriteLogCommand>()
        When("invoke execute") {
            looper.addCommand(writeLogCommand)
            looper.loop()
            Then("write exception log in file") {
                verify(exactly = 1) {
                    writeLogCommand.execute()
                }
            }
        }
    }

    // 5.
    Given("Command send WriteLogCommand in lopper") {
        val command = spyk(LogExceptionCommand())
        When("WriteLogCommand execute in loop") {
            looper.addCommand(command)
            looper.loop()

            Then("WriteLogCommand logged exception") {
                verify(exactly = 1) {
                    command.execute()
                }
            }
        }
    }

    //6-7.
    Given("Command to be processed again") {
        val command = spyk(TryExceptionCommand())
        When("throw exception in loop") {
            looper.addCommand(command)
            looper.loop()

            Then("send command to invoke previous command") {
                verify(exactly = 2) { command.execute() }
            }
        }
    }

    // 8.
    Given("Command to be processed again or logged") {
        val command = spyk(OnceTryOrLogExceptionCommand())
        When("processed again and throw exception") {
            looper.addCommand(command)
            looper.loop()

            Then("command execute twice send logged command") {
                verify(exactly = 2) { command.execute() }
            }
        }
    }

    // 9.
    Given("Command to processed twice or logged") {
        val command = spyk(DoubleTryOrLogExceptionCommand())
        When("processed twice and throw exception") {
            looper.addCommand(command)
            looper.loop()

            Then("command execute thrice send logged command") {
                verify(exactly = 3) { command.execute() }
            }
        }
    }
})
