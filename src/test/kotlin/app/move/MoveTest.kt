package app.move

import app.command.move.MoveAdapter
import app.command.move.MoveCommand
import app.ships.TestShip
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

class MoveTest : BehaviorSpec({

    Given("Ship in position <x, y>") {
        val ship = TestShip(
            mutableMapOf(
                "position" to Vector2D(12.0, 5.0),
                "angularVelocity" to Vector2D(3.0, -1.0),
                "velocity" to Vector2D(-7.0, 3.0),
                "direction" to 0,
                "directionNumber" to 0,
            )
        )
        When("invoke move command") {
            Then("ship move to <dx, dy> position") {
                val moveAdapter = MoveAdapter(ship)
                MoveCommand(moveAdapter).execute()
                val position =
                    (ship.getProperty("position") as Vector2D).toPair()
                position shouldBe Pair(5.0, 8.0)
            }
        }
    }

    Given("Ship with trash arguments") {
        val ship = TestShip(
            mutableMapOf(
                "" to 35f,
                "234" to 0.0
            )
        )
        When("invoke get position") {
            Then("throw exception") {
                shouldThrow<Exception> {
                    ship.getProperty("position")
                }
            }
        }

        When("invoke get velocity") {
            Then("throw exception") {
                shouldThrow<Exception> {
                    ship.getProperty("velocity")
                }
            }
        }

        When ("move ship") {
            val adapter = MoveAdapter(ship)
            Then("throw exception") {
                shouldThrow<Exception> {
                    MoveCommand(adapter).execute()
                }
            }
        }
    }
})

private fun Vector2D.toPair(): Pair<Double, Double> {
    return Pair(this.x, this.y)
}

