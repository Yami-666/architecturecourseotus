package math.utils

import java.math.RoundingMode
import kotlin.math.abs

fun Double.divWith(other: Double, numberOfZeroes: Int = 3): Double {
    return if (other != 0.0) {
        val pow = ((this).toBigDecimal() / (other).toBigDecimal()).toDouble()
        pow.formatTo(numberOfZeroes)
    } else {
        throw ArithmeticException("Division by zero!")
    }
}

fun Double.formatTo(numberOfZeroes: Int): Double {
    return this.toBigDecimal().setScale(numberOfZeroes, RoundingMode.CEILING).toDouble()
}

fun Double.timesWith(other: Double, numberOfZeroes: Int = 3): Double {
    val pow = ((this).toBigDecimal() * (other).toBigDecimal()).toDouble()
    return pow.formatTo(numberOfZeroes)
}

fun Double?.sumWith(other: Double?, numberOfZeroes: Int = 3): Double {
    val sum = ((this ?: 0.0).toBigDecimal() + (other ?: 0.0).toBigDecimal()).toDouble()
    return sum.formatTo(numberOfZeroes)
}

fun Double?.minWith(other: Double?, numberOfZeroes: Int = 3): Double {
    val sub = ((this ?: 0.0).toBigDecimal() - (other ?: 0.0).toBigDecimal()).toDouble()
    return sub.formatTo(numberOfZeroes)
}

fun Double.equalsWithEpsilon(other: Double, epsilon: Double) = abs(this - other) < epsilon

fun List<Double>.equalsWithEpsilon(other: List<Double>, epsilon: Double): Boolean {
    return this.zip(other) { a, b ->
        abs(a - b) < epsilon
    }.all { it }
}