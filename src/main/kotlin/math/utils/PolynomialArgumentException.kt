package math.utils

class PolynomialArgumentException : Exception(POLYNOM_EXC_MSG)

const val POLYNOM_EXC_MSG = "Polynomial argument exception"