package math

interface IPolynomial {
    fun solve(a: Double, b: Double, c: Double, epsilon: Double = 0.0001): List<Double>
}