package math

import math.utils.*
import kotlin.math.pow
import kotlin.math.sqrt

internal class Polynomial : IPolynomial {
    @Throws(PolynomialArgumentException::class)
    override fun solve(a: Double, b: Double, c: Double, epsilon: Double): List<Double> {
        checkPolynomialArguments(a, b, c, epsilon)

        val discriminant = b.pow(2) - 4.0.timesWith(a).timesWith(c)
        return when {
            discriminant.equalsWithEpsilon(0.0, epsilon) -> computeRoot(b, a)
            discriminant > epsilon -> computeTwoRoots(a, discriminant, b)
            else -> emptyList()
        }
    }

    private fun checkPolynomialArguments(
        a: Double,
        b: Double,
        c: Double,
        epsilon: Double,
    ) {
        if (a.equalsWithEpsilon(0.0, epsilon)) throw PolynomialArgumentException()
        if (a.isInfinite() || b.isInfinite() || c.isInfinite()) throw PolynomialArgumentException()
        if (a.isNaN() || b.isNaN() || c.isNaN()) throw PolynomialArgumentException()
    }

    private fun computeTwoRoots(
        a: Double,
        discriminant: Double,
        b: Double
    ): List<Double> {
        val divider = 2.0.timesWith(a)
        val sqrtByDiscriminant = sqrt(discriminant)

        val x1 = (-b.minWith(sqrtByDiscriminant)).divWith(divider)
        val x2 = (-b.sumWith(sqrtByDiscriminant)).divWith(divider)
        return listOf(x1, x2)
    }

    private fun computeRoot(
        b: Double,
        a: Double
    ): List<Double> {
        val x1 = -b.divWith(2.0.timesWith(a))
        return listOf(x1, x1)
    }
}