package app.command

interface ICommand {
    fun execute()
}