package app.command.rotate

import app.ships.IUObject
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

@Suppress("UNCHECKED_CAST")
class RotateAdapter(
    private val obj: IUObject
): IRotable {
    override fun getDirection(): Int {
        return obj.getProperty("direction") as Int
    }

    override fun setDirection(newDirection: Int) {
        return obj.setProperty("direction", Int)
    }

    override fun getDirectionNumber(): Int {
        return obj.getProperty("directionNumber") as Int
    }

    override fun getAngularVelocity(): Vector2D {
        return obj.getProperty("angularVelocity") as Vector2D
    }
}