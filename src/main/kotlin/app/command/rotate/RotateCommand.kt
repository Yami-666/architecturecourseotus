package app.command.rotate

import app.command.ICommand
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

class RotateCommand(
    private val rotable: IRotable
) : ICommand {
    override fun execute() {
        val angularVelocity = rotable.getAngularVelocity()
        val directionVector = Vector2D(
            rotable.getDirection().toDouble(),
            rotable.getDirection().toDouble(),
        )
        val directionNumber = rotable.getDirectionNumber()
        val newDirection = 0
        rotable.setDirection(newDirection)
    }
}
