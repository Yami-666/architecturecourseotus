package app.command.rotate

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

interface IRotable {
    fun getDirection(): Int
    fun setDirection(newDirection: Int)
    fun getDirectionNumber(): Int
    fun getAngularVelocity(): Vector2D
}