package app.command.move

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

interface IMovable {
    fun getPosition(): Vector2D
    fun setPosition(position: Vector2D)
    fun getVelocity(): Vector2D
}