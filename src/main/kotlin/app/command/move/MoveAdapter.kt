package app.command.move

import app.command.exceptions.PositionException
import app.command.exceptions.VelocityException
import app.command.move.IMovable
import app.ships.IUObject
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

@Suppress("UNCHECKED_CAST")
class MoveAdapter(
    private val obj: IUObject
) : IMovable {

    override fun getPosition(): Vector2D {
        return (obj.getProperty("position") as? Vector2D)
            ?: throw PositionException()
    }

    override fun setPosition(position: Vector2D) {
        obj.setProperty("position", position)
    }

    override fun getVelocity(): Vector2D {
        return obj.getProperty("velocity") as? Vector2D
            ?: throw VelocityException()
    }
}