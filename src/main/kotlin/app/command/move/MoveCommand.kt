package app.command.move

import app.command.ICommand

class MoveCommand(
    private val movable: IMovable
) : ICommand {
    override fun execute() {
        val newPosition = movable.getPosition().add(movable.getVelocity())
        movable.setPosition(newPosition)
    }
}