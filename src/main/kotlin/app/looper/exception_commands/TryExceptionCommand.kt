package app.looper.exception_commands

import app.command.ICommand

class TryExceptionCommand : ICommand {
    override fun execute() {
        println("TryExceptionExecute execute")
        throw Exception()
    }
}