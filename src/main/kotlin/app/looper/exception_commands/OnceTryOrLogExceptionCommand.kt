package app.looper.exception_commands

import app.command.ICommand

class OnceTryOrLogExceptionCommand : ICommand {
    override fun execute() {
        println("TryOrLogException execute")
        throw Exception()
    }
}