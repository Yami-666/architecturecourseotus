package app.looper.exception_commands

import app.command.ICommand

class LogExceptionCommand : ICommand {
    override fun execute() {
        println("LogException execute")
        throw Exception()
    }
}