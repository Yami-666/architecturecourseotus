package app.looper.exception_commands

import app.command.ICommand

class DoubleTryOrLogExceptionCommand : ICommand {
    override fun execute() {
        println("TryTryOrLogException execute")
        throw Exception()
    }
}