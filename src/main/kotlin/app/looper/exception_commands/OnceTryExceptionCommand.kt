package app.looper.exception_commands

import app.command.ICommand

class OnceTryExceptionCommand : ICommand {
    override fun execute() {
        println("TryOrLogException execute")
        throw Exception()
    }
}