package app.looper.exception_commands

import app.command.ICommand

class DoubleTryExceptionCommand : ICommand {
    override fun execute() {
        println("TryTryOrLogException execute")
        throw Exception()
    }
}