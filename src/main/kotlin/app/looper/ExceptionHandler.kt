package app.looper

import app.command.ICommand
import app.looper.exception_commands.LogExceptionCommand
import app.looper.exception_commands.TryExceptionCommand
import app.looper.exception_commands.OnceTryOrLogExceptionCommand
import app.looper.exception_commands.DoubleTryOrLogExceptionCommand

class ExceptionHandler(
    private val looper: EventLooper
) : IExceptionHandler {

    private val hashTable = hashMapOf<String, (ICommand, Exception) -> Unit>(
        // Записывать log исключения
        LogExceptionCommand::class.java.simpleName to { _, exception ->
            looper.addCommand(WriteLogCommand(exception))
        },
        // Отправляем TryAgainCommand
        TryExceptionCommand::class.java.simpleName to { command, _ ->
            looper.addCommand(TryAgainCommand(command))
        },
        // Пробуем выполнить ещё раз
        OnceTryOrLogExceptionCommand::class.java.simpleName to { command, _ ->
            looper.addCommand(TryAgainOrWriteCommand(command))
        },
        // Пробуем выполнить два раза
        DoubleTryOrLogExceptionCommand::class.java.simpleName to { command, _ ->
            looper.addCommand(TryTryAgainOrWriteCommand(command))
        },
        // Если не получилось выполнить ещё раз, записываем log
        TryAgainOrWriteCommand::class.java.simpleName to { _, exception ->
            looper.addCommand(WriteLogCommand(exception))
        },
        // Если не удалось выполнить в первый раз, пытаемся выполнить ещё раз
        TryTryAgainOrWriteCommand::class.java.simpleName to { command, _ ->
            looper.addCommand(TryAgainOrWriteCommand(command))
        },
    )

    override fun handle(command: ICommand, exception: Exception) {
        val hashId = command.javaClass.simpleName
        hashTable[hashId]?.invoke(command, exception)
    }
}