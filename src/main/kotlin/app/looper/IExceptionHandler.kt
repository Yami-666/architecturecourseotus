package app.looper

import app.command.ICommand

interface IExceptionHandler {
    fun handle(command: ICommand, exception: Exception)
}