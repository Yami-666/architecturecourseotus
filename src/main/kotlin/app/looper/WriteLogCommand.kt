package app.looper

import app.command.ICommand
import java.io.File

class WriteLogCommand(
    private val exception: Exception,
    private val logFile: File = File("/home/user/IdeaProjects/ArchitectureCourseOTUS/src/main/resources/log.txt")
) : ICommand {
    override fun execute() {
        if (!logFile.exists()) logFile.createNewFile()
        println("throw $exception in command looper")
        logFile.writeText("$exception")
    }
}