package app.looper

import app.command.ICommand
import java.util.LinkedList
import java.util.Queue

class EventLooper : IEventLooper {
    private val queue: Queue<ICommand> = LinkedList()
    private val exceptionHandler: IExceptionHandler = ExceptionHandler(this)

    override fun loop() {
        while (queue.isNotEmpty()) {
            val command = queue.poll()
            try {
                command?.execute()
            } catch (e: Exception) {
                exceptionHandler.handle(command, e)
            }
        }
    }

    override fun addCommand(command: ICommand) {
        queue.add(command)
    }
}

interface IEventLooper {
    fun loop()
    fun addCommand(command: ICommand)
}