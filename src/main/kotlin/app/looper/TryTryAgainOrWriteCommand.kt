package app.looper

import app.command.ICommand

class TryTryAgainOrWriteCommand(
    private val command: ICommand
) : ICommand {
    override fun execute() {
        command.execute()
    }
}