package app.looper

import app.command.ICommand

class TryAgainOrWriteCommand(
    private val command: ICommand
) : ICommand {
    override fun execute() {
        command.execute()
    }
}