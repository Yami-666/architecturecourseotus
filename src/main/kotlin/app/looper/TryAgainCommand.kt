package app.looper

import app.command.ICommand

class TryAgainCommand(
    private val command: ICommand
) : ICommand {
    override fun execute() {
        command.execute()
    }
}