package app.ships

class TestShip(
    private val fields: MutableMap<String, Any>
): IUObject {

    override fun getProperty(key: String): Any {
        return fields[key]!!
    }

    override fun setProperty(key: String, newValue: Any) {
        fields[key] = newValue
    }
}