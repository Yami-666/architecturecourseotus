package app.ships

interface IUObject {
    fun getProperty(key: String): Any
    fun setProperty(key: String, newValue: Any)
}